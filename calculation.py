# -*- coding: utf-8 -*-
"""
Created on Tue Nov 28 11:35:07 2017

@author: Brian
"""
import math

class knnCalc:
    
    def distance(x1, y1, x2, y2):
    # menghitung jarak antara dua poin
        return math.sqrt(math.pow(x1-x2, 2) + math.pow(y1-y2, 2))
        # hitung jarak pakai rumus pythagoras dan return hasilnya
    
    def kNN(k, t, cl1, cl2):
        # k = konstanta kNN
        # t = poin yang ingin diketahui classnya
        # cl1 = array poin di class 1
        # cl2 = array poin di class 2
        obj1 = []
        # obj1 = menyimpan jarak antara poin t dengan setiap poin di class 1; kosongkan
        for obj in cl1:
        #untuk setiap poin di cl1 sebagai obj
            obj1.append(calculation.distance(obj[0], obj[1], t[0], t[1]))
            # hitung jarak antara obj dan t dan simpan di obj1 indeks ke-i
        
        obj2 = []
        # obj2 = menyimpan jarak antara poin t dengan setiap poin di class 2; kosongkan
        
        for obj in cl2:
        #untuk setiap poin di cl2 sebagai obj
            obj2.append(calculation.distance(obj[0], obj[1], t[0], t[1]))
            # hitung jarak antara obj dan t dan simpan di obj2 indeks ke-i
            
        obj1 = sorted(obj1, key=int, reverse=True)
        # urutkan array obj1 dari yang terbesar ke terkecil
        obj2 = sorted(obj2, key=int, reverse=True)
        # urutkan array obj1 dari yang terbesar ke terkecil
        
        c1 = 0
        # c1 = jumlah poin terdekat dari t ke class 1
        c2 = 0
        # c2 = jumlah poin terdekat dari t ke class 2
        
        i = 0
        # i = indeks untuk obj1
        j = 0
        # j = indeks untuk obj2
        
        for x in range(0, k):
        # proses diulang k kali agar mendapan k poin terdekat dari poin t
            if (obj1[i] < obj2[j] and obj1[i] != None and obj2[i] != None):
            # jika obj1 indeks ke-i lebih kecil dari obj2 indeks ke-j
                ++c1
                # inkremen c1
                ++i
                # inkremen indeks i untuk pass ke obj1 indeks selanjutnya
            elif (obj1[i] > obj2[j] and obj1[i] != None and obj2[i] != None):
            # jika obj2 indeks ke-i lebih kecil dari obj1 indeks ke-j
                ++c2
                # inkremen c2
                ++j
                # inkremen indeks j untuk pass ke obj2 indeks selanjutnya
            elif (obj1[i] == obj2[j] and obj1[i] != None and obj2[i] != None):
                ++j
                ++i
                # inkremen indeks i dan j untuk skip ke indeks selanjutnya jika kedua nilai sama
            elif (obj1[i] == None):
            # jika obj1 indeks ke i kosong
                ++c2
                # inkremen c2
                ++j
                # inkremen indeks j untuk skip ke indeks selanjutnya
            elif (obj2[j] == None):
            # jika obj2 indeks ke j kosong
                ++c1
                # inkremen c1
                ++i
                # inkremen indeks i untuk skip ke indeks selanjutnya
            else:
                break
        
        ans: ""
        # ans: keterangan class yang mana
        if c1 > c2:
        # jika c1 lebih besar dari c2
            ans = "class 1"
            # assign ans dengan "class 1"
        elif c1 < c2:
        # jika c1 lebih kecil dari c2
            ans = "class 2"
            # assign ans dengan "class 2"
        else:
            ans = "unknown class"
            # assign ans dengan "unknown class" jika class tidak diketahui karena jarak sama
        
        print(t, " is in ", ans)
        # print jawaban
            